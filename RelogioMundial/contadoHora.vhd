library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity contadordezenaMinuto is
   
   port (
			clock   : in  std_logic;
			enable  : in  std_logic;									
			reset  	: in  std_logic;							
			Q       : out std_logic_vector(3 downto 0);
			RCO     : out std_logic	
    );

end contadordezenaMinuto;

architecture Contador of contadordezenaMinuto is
signal IQ: unsigned (3 downto 0);
begin

	process (clock, reset, enable, IQ)
	begin
		if reset = '1' then
			IQ <= (others => '0');	
		elsif clock'event and clock = '1' then
			if enable = '1' then 
				if IQ >= 5 then
					IQ <= (others => '0');
					RCO <= '1';
				else
					IQ <= IQ + 1;
					RCO <= '0';
					end if;
			end if;
		end if;
		Q <= std_logic_vector(IQ);     
	end process;
	
end Contador;