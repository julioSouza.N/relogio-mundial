
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

----- criar unidade de controle
------ tentar criar logica para editar a hora 
entity fluxo_dados is
 port(
      clock	 : in   std_logic;
		reset	 : in   std_logic;
		enable : in std_logic; 
		FusoH 	: in   std_logic;

		led: out std_logic;
		anodes  	: out std_logic_vector(3 downto 0);
		seg      : out std_logic_vector(6 downto 0)
	);
end fluxo_dados;

architecture Behavioral of fluxo_dados is

----- Componentes 
------ Minutos

component ParteTempo is
 port(
      clock	: in   std_logic;
		reset	: in   std_logic;
		enable : in std_logic; 
		sinalStartMinuto: out std_logic
	);
end component;

	

component ParteMinuto is
 port(
      clock					: in   std_logic;
		reset					: in   std_logic;
		sinalIniciaMinuto : in   std_logic;
		parteUnidadeMinuto: out  std_logic_vector(6 downto 0);
		parteDezenaMinuto : out  std_logic_vector(6 downto 0);
		RCO_Hora				: out  std_logic
	);
end component;


---- Horas
component ParteHora is
 port(
      clock	: in   std_logic;
		reset	: in   std_logic;
		enable : in std_logic; 
		FusoH 	: in   std_logic;

		led: out std_logic;
		startContagemHora	 : in std_logic;
		unidadeHora     	 : out std_logic_vector(6 downto 0);
		dezenaHora    	  	 : out std_logic_vector(6 downto 0)
	);
end component;


component display is
    port(
        clock     : in  std_logic;
		  entrada3  : in  std_logic_vector(6 downto 0);
		  entrada2  : in  std_logic_vector(6 downto 0);
		  entrada1  : in  std_logic_vector(6 downto 0);
		  entrada0  : in  std_logic_vector(6 downto 0);
        sevenseg  : out std_logic_vector(6 downto 0);
        anodes  	: out std_logic_vector(3 downto 0));
end component;
	
	
	------ entrados Horas
	signal RCO_Hora				: std_logic;

	signal parteUnidadeHora   	: std_logic_vector(6 downto 0);
	signal parteDezenaHora    	: std_logic_vector(6 downto 0);
	
	signal parteUnidadeMinuto   	: std_logic_vector(6 downto 0);
	signal parteDezenaMinuto    	: std_logic_vector(6 downto 0);

	signal SinalSaidaTempo		: std_logic;
	
begin
-----TODO  mudar ClockDividio para enable (normalizar os paramentors)
	
	tempo : ParteTempo port map(clock,reset,enable,SinalSaidaTempo);

-------- parte para mostra minutos ---------------

	parteDoMinuto : ParteMinuto port map(clock,reset,SinalSaidaTempo,parteUnidadeMinuto,parteDezenaMinuto,RCO_Hora);
	
	parteDoHora : ParteHora port map(clock,reset,enable,FusoH,led,RCO_Hora,parteUnidadeHora,parteDezenaHora);

	------TODO corrigir nomes dos paramentors para hora 
	
	dis  :display port map (clock,parteUnidadeMinuto,parteDezenaMinuto,parteDezenaHora,parteUnidadeHora,seg,anodes);
	

			
					
end Behavioral;
