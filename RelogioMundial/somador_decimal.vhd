library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity somador_decimal is
    Port ( HoraBrasil : in  STD_LOGIC_VECTOR (4 downto 0);
           FusoHorario : in  STD_LOGIC_VECTOR (4 downto 0);
           cin : in  STD_LOGIC;
           HoraComFusoHorario : out  STD_LOGIC_VECTOR (4 downto 0);
           cout : out  STD_LOGIC);
end somador_decimal;

 architecture Behavioral of somador_decimal is
	signal AuxSomaFuso: STD_LOGIC_VECTOR (4 downto 0);
	signal Sum: unsigned (4 downto 0) := "00000";

begin

	Sum(0) <= HoraBrasil(0) xor FusoHorario(0) xor cin;
	AuxSomaFuso(0) <= (HoraBrasil(0) and FusoHorario(0)) or ((HoraBrasil(0) xor FusoHorario(0)) and cin);
	Sum(1) <= HoraBrasil(1) xor FusoHorario(1) xor AuxSomaFuso(0);
	AuxSomaFuso(1) <= (HoraBrasil(1) and FusoHorario(1)) or ((HoraBrasil(1) xor FusoHorario(1)) and AuxSomaFuso(0));
	Sum(2) <= HoraBrasil(2) xor FusoHorario(2) xor AuxSomaFuso(1);
	AuxSomaFuso(2) <= (HoraBrasil(2) and FusoHorario(2)) or ((HoraBrasil(2) xor FusoHorario(2)) and AuxSomaFuso(1));
	Sum(3) <= HoraBrasil(3) xor FusoHorario(3) xor AuxSomaFuso(2);
	AuxSomaFuso(3)  <= (HoraBrasil(3) and FusoHorario(3)) or ((HoraBrasil(3) xor FusoHorario(3)) and AuxSomaFuso(2));
	Sum(4) <= HoraBrasil(4) xor FusoHorario(4) xor AuxSomaFuso(3);
	AuxSomaFuso(4)  <= (HoraBrasil(4) and FusoHorario(4)) or ((HoraBrasil(4) xor FusoHorario(4)) and AuxSomaFuso(4));
	
	HoraComFusoHorario <= std_logic_vector(Sum - 24) when Sum > 23 else
		  std_logic_vector(Sum + 8) when AuxSomaFuso(4) = '1' else
		  std_logic_vector(Sum);
		  
   cout <= '1' when Sum > 23 else
           AuxSomaFuso(4);

end Behavioral;

