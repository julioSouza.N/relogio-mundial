-- VHDL do detector de borda

library ieee;
use ieee.std_logic_1164.all;

entity detector_borda is
   port(
      clock    : in   std_logic;
		reset		: in   std_logic;
      botao	   : in   std_logic;
		borda    : out  std_logic
	);
end detector_borda;

architecture exemplo of detector_borda is
type tipo_estado is (s0, s1, s2);
signal estado   : tipo_estado;
   
begin
	process (clock, estado, reset)
	begin
   
	if reset = '1' then
		estado <= s0;
         
	elsif clock'event and clock = '1' then
		case estado is
			when s0 =>
				if botao = '1' then
					estado <= s1;
				end if;

			when s1 =>
				estado <= s2;
				
			when s2 =>
				if botao = '0' then
					estado <= s0;
				end if; 
				
		end case;
	end if;
	end process;
   
	with estado select borda <=
		'1' when s1,
		'0' when others;
		
end exemplo;