
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity ParteTempo is
 port(
      clock	: in   std_logic;
		
		reset	: in   std_logic;
		
		enable : in std_logic; 
		
		sinalStartMinuto: out std_logic
	);
end ParteTempo;

architecture Behavioral of ParteTempo is

component divclock is
    port (
		  clock     : in  std_logic;
		  clear		: in  std_logic;
		  enable		: in  std_logic;
        clock_div : out std_logic
    );
end component ;

component detector_borda is
   port(
      clock    : in   std_logic;
		reset		: in   std_logic;
      botao	   : in   std_logic;
		borda    : out  std_logic
	);
end component;

signal ClockDividio 	: std_logic;

begin 

   div  : divclock port map (clock, reset,enable,ClockDividio); --divide o clock para minutos entradas clock,reset,enable da maquina saida clockdiv
	
	borad : detector_borda port map (clock,reset,ClockDividio,sinalStartMinuto); -- detector de borda de subida entrados clock,reset,clockdiv  saida sinal


end Behavioral;