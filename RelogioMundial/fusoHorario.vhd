
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity fusoHorario is
	port (
      
        enable: in  std_logic;									
		  horaDoBrasil       : in std_logic_vector(4 downto 0);
		  horaDoPais         : out std_logic_vector(4 downto 0)
    );
end fusoHorario;

architecture Behavioral of fusoHorario is

signal IQ: unsigned (3 downto 0) := "0011";

begin

process (enable, horaDoBrasil,IQ)
	begin
		if enable = '0' then
			horaDoPais <= horaDoBrasil;
		else 
			horaDoPais <= horaDoBrasil + IQ;
		end if;
		
	end process;
	
end Behavioral;

