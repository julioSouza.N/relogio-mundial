
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Top is
 port(
      clock	 	: in   std_logic;
		reset	 	: in   std_logic;
		enable 	: in   std_logic; 
		FusoH : in   std_logic;

		led		: out  std_logic;
		anodes  	: out  std_logic_vector(3 downto 0);
		seg      : out  std_logic_vector(6 downto 0)
	);
end Top;

architecture Behavioral of Top is

component uc is
   port(
      clock	: in   std_logic;
		reset	: in   std_logic;
		start	: in	 std_logic;		
		en1	: out  std_logic;
		dep	: out  std_logic_vector(1 downto 0)	-- Saida de depuracao
	);
end component;

component fluxo_dados is
 port(
      clock	 : in   std_logic;
		reset	 : in   std_logic;
		enable : in std_logic;
		FusoH 	: in   std_logic;

		led: out std_logic;
		anodes  	: out std_logic_vector(3 downto 0);
		seg      : out std_logic_vector(6 downto 0)
	);
end component;

	signal inicio :std_logic;
   signal lixo 	: std_logic_vector(1 downto 0);
	
begin

unidadecontrole :uc  port map (clock,reset,enable,inicio,lixo);
fluxodado :fluxo_dados port map (clock,reset,enable,FusoH,led,anodes,seg);	


end Behavioral;

