library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity divclockH is
    port (
		  clock     : in  std_logic;
		  clear		: in  std_logic;
		  enable		: in  std_logic;
        clock_div : out std_logic
    );
end divclockH;

architecture exemplo of divclockH is
	signal IQ	: unsigned(37 downto 0);
	signal pivo	: std_logic;
begin
	
	process (clock, clear, enable, IQ, pivo)
	begin
	
	if clear = '1' then
		IQ   <= (others => '0');
		pivo <= '0';

	elsif clock'event and clock = '1' then
		if enable = '1' then
			IQ <= IQ + 1;
		
			if IQ = 25000000 then --25000000 --1500000000 --90000000000
				pivo <= not(pivo);
				IQ   <= (others => '0');
			end if;
		end if;
	end if;
	 
	clock_div <= pivo;
	end process;
	
end exemplo;




