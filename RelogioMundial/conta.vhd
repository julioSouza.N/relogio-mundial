
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;



entity conta is
 port(
			 clock  : in  std_logic;
			 Ligaled :in std_logic;
			 led: out std_logic;
			 horasBrasil      : in  std_logic_vector(4 downto 0);
			 horasSida        : out  std_logic_vector(4 downto 0)
			
			);				
end conta;

architecture Behavioral of conta is
signal fusoHorario: std_logic_vector (4 downto 0);
signal soma: std_logic_vector (4 downto 0) := "00100";
signal sainda : std_logic_vector (4 downto 0) := "00000";

begin

	 fusoHorario <= soma xor horasBrasil ;
	 
	 process (clock,Ligaled,sainda,fusoHorario)
	 begin 
	 
	 	if clock'event and clock = '1' then
			if Ligaled = '1' then
				sainda <= fusoHorario;
			end if ;
		end if;
	 
	 end process;

	horasSida <= sainda;
	led <= Ligaled;
	
end Behavioral;

