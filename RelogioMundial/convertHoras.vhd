-- VHDL do conversor para horas
-- O codigo recebe um binario de 5 bits (2^5 >= 24) e converte para duas saidas
-- hexadecimais correspondentes ao valor em horas. Estas duas saidas deverao
-- passar pelos conversores para 7 segmentos, de maneira que elas sejam apresentadas
-- nos displays da placa.
-- OBS: Voce precisa garantir que o seu contador responsavel pelas horas zere
-- a contage na proxima borda de clock apos atingir o valor 24

library ieee;
use ieee.std_logic_1164.all;

entity convhoras is
   port(
      horas    : in   std_logic_vector(4 downto 0);
		hex0		: out  std_logic_vector(3 downto 0);
		hex1     : out  std_logic_vector(3 downto 0)
	);
end convhoras;

architecture exemplo of convhoras is
begin

	hex1 <= "0010" when horas > "10011" and horas < "11001" else  -- 19 < horas < 25
			  "0001" when horas > "01001" and horas < "10100" else  -- 9  < horas < 20
			  "0000";																  -- horas < 9
			  
	hex0 <= "0000" when horas = "00000" or horas = "01010" or horas = "10100" else	-- 0, 10, 20 
			  "0001" when horas = "00001" or horas = "01011" or horas = "10101" else	-- 1, 11, 21
			  "0010" when horas = "00010" or horas = "01100" or horas = "10110" else	-- 2, 12, 22
			  "0011" when horas = "00011" or horas = "01101" or horas = "10111" else	-- 3, 13, 23
			  "0100" when horas = "00100" or horas = "01110" or horas = "11000" else	-- 4, 14, 24
			  "0101" when horas = "00101" or horas = "01111" else	-- 5, 15
			  "0110" when horas = "00110" or horas = "10000" else	-- 6, 16
			  "0111" when horas = "00111" or horas = "10001" else	-- 7, 17
			  "1000" when horas = "01000" or horas = "10010" else	-- 8, 18
			  "1001" when horas = "01001" or horas = "10011" else	-- 9, 18
			  "0000";
end exemplo;