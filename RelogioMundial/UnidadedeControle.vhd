-- VHDL de uma Unidade de Controle para o circuito de soma de dois numeros com
-- saida registrada

library ieee;
use ieee.std_logic_1164.all;

entity uc is
   port(
      clock	: in   std_logic;
		reset	: in   std_logic;
		start	: in	 std_logic;		
		en1	: out  std_logic;
		dep	: out  std_logic_vector(1 downto 0)	-- Saida de depuracao
	);
end uc;

architecture exemplo of uc is
	type tipo_estado is (inicial, contando);
	signal estado   : tipo_estado; 
begin
	
	-- ================================================
	-- Logica de transicao de estados (sequencial)
	-- ================================================
	process (clock, reset, estado, start)
	begin
   
	-- O sinal de reset eh necessario para garantir a condicao de estado inicial
	if reset = '1' then
		estado <= inicial;
         
	elsif (clock'event and clock = '1') then
	
				if start = '1' then
					estado <= contando;
				else
					estado <= inicial;
				end if;

	end if;
	
	end process;
   
	-- ================================================
	-- Logica de saida (combinatoria)
	-- ================================================
	with estado select en1 <=
		'1' when contando,
		'0' when others;	
		
	with estado select dep <=
		"00" when inicial,
		"01" when contando,
		"11" when others;


end exemplo; 