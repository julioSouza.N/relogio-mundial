
# PlanAhead Launch Script for Post-Synthesis floorplanning, created by Project Navigator

create_project -name RelogioMundial -dir "E:/Relogio Mundial/RelogioMundial/planAhead_run_3" -part xc3s100ecp132-4
set_property design_mode GateLvl [get_property srcset [current_run -impl]]
set_property edif_top_file "E:/Relogio Mundial/RelogioMundial/fluxo_dados.ngc" [ get_property srcset [ current_run ] ]
add_files -norecurse { {E:/Relogio Mundial/RelogioMundial} }
set_property target_constrs_file "Pinos.ucf" [current_fileset -constrset]
add_files [list {Pinos.ucf}] -fileset [get_property constrset [current_run]]
link_design
