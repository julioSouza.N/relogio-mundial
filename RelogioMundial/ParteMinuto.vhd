
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity ParteMinuto is
 port(
      clock					: in   std_logic;
		reset					: in   std_logic;
		sinalIniciaMinuto : in std_logic;
		parteUnidadeMinuto: out std_logic_vector(6 downto 0);
		parteDezenaMinuto : out std_logic_vector(6 downto 0);
		RCO_Hora				: out   std_logic
	);
end ParteMinuto;

architecture Behavioral of ParteMinuto is

component ContadorMinutoUnidade is
    port (
	 
			 clock   : in  std_logic;
			 enable  : in  std_logic;			 
			 reset  	: in  std_logic;		 
          Q       : out std_logic_vector(3 downto 0);
		    RCO     : out std_logic									
    
	 );
	end component;
	
component contadordezenaMinuto is
    port (
			 clock   : in  std_logic;
			 enable  : in  std_logic;			 
			 reset  	: in  std_logic;		 
          Q       : out std_logic_vector(3 downto 0);
		    RCO     : out std_logic								
    );
end component;



component hex7seg is
	port (hex      : in  std_logic_vector(3 downto 0);
         display  : out std_logic_vector(6 downto 0)
          );
end component;


signal minutoUnidade			: std_logic_vector(3 downto 0);
signal minutoDezena 			: std_logic_vector(3 downto 0);
signal ClockDividio 			: std_logic;
signal RCO 						: std_logic;
begin 

   														    --  in	  in 	   in     out		out 
	unidadeMinutos	: ContadorMinutoUnidade port map(clock,sinalIniciaMinuto,reset,minutoUnidade, RCO); -- conta unidade do minuto 0X
																--  in	 in 	 in  out  out 
	dezenaMinuto  :  contadordezenaMinuto port map(RCO,sinalIniciaMinuto,reset,minutoDezena,RCO_Hora); -- conta parte decimal do minuto X0
	
	hex0 : hex7seg port map (minutoUnidade,parteUnidadeMinuto);
	hex1 : hex7seg port map (minutoDezena,parteDezenaMinuto);
	


end Behavioral;
