library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


----- criar unidade de controle
------ tentar criar logica para editar a hora 
entity ParteHora is
 port(
      clock	    : in   std_logic;
		reset	    : in   std_logic;
		enable    : in std_logic; 
		FusoH : in   std_logic;

		led: out std_logic;
		startContagemHora	 : in std_logic;
		unidadeHora     	 : out std_logic_vector(6 downto 0);
		dezenaHora    	  	 : out std_logic_vector(6 downto 0)
	);
end ParteHora;

architecture Behavioral of ParteHora is

-------------
component hex7seg is
	port (hex      : in  std_logic_vector(3 downto 0);
         display  : out std_logic_vector(6 downto 0)
          );
end component;

component ContadorUnidadeHora is
    port (
		 -- Ligaled :in std_logic;
		--  led: out std_logic;
        clock  : in  std_logic;
        enable : in  std_logic;									
        reset	 : in  std_logic;	
        Q       : out std_logic_vector(4 downto 0);
		  RCO     : out std_logic	
    );
end component;

component convhoras is
   port(
      horas    : in   std_logic_vector(4 downto 0);
		hex0		: out  std_logic_vector(3 downto 0);
		hex1     : out  std_logic_vector(3 downto 0)
	);
end component;



component somador_decimal is
  Port (   HoraBrasil : in  STD_LOGIC_VECTOR (4 downto 0);
           FusoHorario : in  STD_LOGIC_VECTOR (4 downto 0);
           cin : in  STD_LOGIC;
           HoraComFusoHorario : out  STD_LOGIC_VECTOR (4 downto 0);
           cout : out  STD_LOGIC);
end component;


	
	signal Horas 					:  std_logic_vector(4 downto 0);
	signal HorasComFusoHorario :  std_logic_vector(4 downto 0);

	signal RCO_lixo				: std_logic;

	signal horaDezena			   : std_logic_vector(3 downto 0);
	signal horaUnidade 			: std_logic_vector(3 downto 0);

	signal Fuso :   std_logic_vector (4 downto 0);
	signal FusoHorario: std_logic_vector (4 downto 0) := "00000";
begin 
		
	contadorHora  :  ContadorUnidadeHora port map(startContagemHora,enable,reset,Horas,RCO_lixo);  --- port map(Ligaled,led,startContagemHora,enable,reset,Horas,RCO_lixo);
	
	process (FusoH,FusoHorario)
	begin 

			if FusoH = '1' then
				FusoHorario <= "00011";
			elsif FusoH = '0' then
				FusoHorario <= "00000";
			end if ;
						
			
	end process ;
	



	Fuso <= FusoHorario;


	addFuso : somador_decimal port map (Horas, FusoHorario, '0', HorasComFusoHorario, led);
	 
	converteParaHora : convhoras port map(HorasComFusoHorario,horaDezena,horaUnidade);

	
	hex0 : hex7seg port map (horaUnidade,unidadeHora);
	hex1 : hex7seg port map (horaDezena,dezenaHora);
	


end Behavioral; 